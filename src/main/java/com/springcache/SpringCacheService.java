package com.springcache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Service
public class SpringCacheService {

	@Autowired
	private SpringCacheDaoImpl springCacheDaoImpl;
	 
	@Autowired 
    private CacheManager cacheManager;  
	
	@Cacheable(value="itemCache", key="#id" )
	public List<String> getItems(Integer id){
		
		System.out.println("getItems called......"); 
	    
	    return springCacheDaoImpl.getItem(id);
	}
	
	 
	public int insertItem(Item item){		
		System.out.println("insertItem called......");		 
	    return springCacheDaoImpl.insertItem(item);
	}
	
	             
    @Scheduled(cron = "0 0 0 1/1 * ? *")               
    public void clearCacheSchedule(){
    	System.out.println("clearCacheSchedule.....");
        for(String name:cacheManager.getCacheNames()){
            cacheManager.getCache(name).clear();            
        }
    }
}
