package com.springcache;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class SpringCacheDaoImpl {

	
	@Autowired
    JdbcTemplate jdbcTemplate;

     
    public List<String> getItem(int id){
        String query = "SELECT name FROM springcache.item WHERE ID="+id;
        return jdbcTemplate.queryForList(query,String.class);
    } 

    public int insertItem(Item item){
    	
    	return jdbcTemplate.update("insert into item values ("+item.id+",'"+item.name+"')");
       
    }
}
