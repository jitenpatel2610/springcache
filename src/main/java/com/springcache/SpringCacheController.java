package com.springcache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/services")
@EnableScheduling
public class SpringCacheController {

	@Autowired
	private SpringCacheService springCacheService;
	
	@RequestMapping(value="/getItems/{id}",method=RequestMethod.GET)	 
	public List<String> getItems(@PathVariable Integer id){
				 
		return springCacheService.getItems(id);
	}
	
	
}

